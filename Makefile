CPPFLAGS = -g -Wall
CXXFLAGS = -Weffc++
src = src/
obj = obj/
bin = bin/
target = hello
objects = hello.o

default: $(target)

.PHONY: default cleanobj clean

%.o: $(src)/%.cpp
	@$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $? -o $(obj)/$@

$(target): $(objects)
	@$(CXX) $(LDFLAGS) $(obj)/$(objects) $(LDLIBS) $(LOADLIBES) -o $(bin)/$@

cleanobj:
	-@rm -f $(obj)/*.o

clean: cleanobj
	-@rm -f $(bin)/$(target)
